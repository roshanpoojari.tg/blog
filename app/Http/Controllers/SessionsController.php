<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function create() {
        return view('sessions.create');
    }

    public function store() {
        $attributes = request()->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if (!auth()->attempt($attributes)) {
            throw ValidationException::withMessages(['email' => 'Your provided credentials does not match.']);
        }

        // regenerate the session to avoid session fixation attacks.
        session()->regenerate();

        // redirect with succes flash message.
        return redirect()->route('home')->with('success', 'Wlecome Back!');
    }

    public function destroy() {
        
        auth()->logout();

        return redirect()->route('home')->with('success', 'Goodbye!');
    }
}
