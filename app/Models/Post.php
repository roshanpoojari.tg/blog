<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $with = ['category', 'author'];

    public function scopeFilter($query, array $filters) {  // can be called like : Post::newQuery()->filter(); where the filter is the after part of the scopeFilter().
        $query->when($filters['search'] ?? false, fn ($query, $search) => 
            $query->where(fn ($query) =>   
                $query
                    ->where('title', 'LIKE', '%'. $search. '%')
                    ->orWhere('body', 'LIKE', '%'. $search. '%')
                )
            );

        $query->when($filters['category'] ?? false, fn ($query, $category) => 
            $query    
                ->whereHas('category', fn ($query) => 
                    $query->where('slug', $category)));
        
            $query->when($filters['author'] ?? false, fn ($query, $author) => 
                $query    
                    ->whereHas('author', fn ($query) => 
                        $query->where('username', $author)));
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
