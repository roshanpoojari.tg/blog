<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $user = User::factory()->create();

        Post::factory(5)->create([
            'user_id' => $user->id,
        ]);

        $category = Category::factory()->create();
        Post::factory(8)->create([
            'category_id' => $category->id,
        ]);

        // $user = User::factory()->create([
        //     'name' => 'Roshan Poojary',
        //     'email' => 'roshan@gmail.com',
        //     'password' => bcrypt('000@Roshan')
        // ]);

        // $personal = Category::create([
        //     'name' => 'Personal' ,
        //     'slug' => 'personal',
        // ]);

        // $work = Category::create([
        //     'name' => 'Work' ,
        //     'slug' => 'work',
        // ]);

        // $family = Category::create([
        //     'name' => 'Family' ,
        //     'slug' => 'family',
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $personal->id,
        //     'slug' => 'my-personal-post',
        //     'title' => 'My Personal Post',
        //     'excerpt' => 'Excerpt for my family post is here.',
        //     'body' => 'Body of family post, will be continuing with a gibrish words huhdu dued euhde dheud edue hdhe uehdu dehdue uhd e heudheuhduehd ueh eh d e',
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $work->id,
        //     'slug' => 'my-work-post',
        //     'title' => 'My Work Post',
        //     'excerpt' => 'Excerpt for my Work post is here.',
        //     'body' => 'Body of work post, will be continuing with a gibrish words huhdu dued euhde dheud edue hdhe uehdu dehdue uhd e heudheuhduehd ueh eh d e',
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $family->id,
        //     'slug' => 'my-family-post',
        //     'title' => 'My Family Post',
        //     'excerpt' => 'Excerpt for my family post is here.',
        //     'body' => 'Body of family post, will be continuing with a gibrish words huhdu dued euhde dheud edue hdhe uehdu dehdue uhd e heudheuhduehd ueh eh d e',
        // ]);
    }
}
