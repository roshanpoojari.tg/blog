@props(['trigger'])

<div x-data="{ show: false }" @click.away="show=false" class="relative">
    {{-- TRIGGER --}}
    <div @click="show = !show">
        {{ $trigger}}
    </div>

    {{-- LINKS --}}
    <div x-show="show" class="absolute bg-gray-100 rounded-xl py-2 mt-2 w-full z-50 overflow-auto max-h-52 " style="display: none">
        {{ $slot }}
    </div>
</div>