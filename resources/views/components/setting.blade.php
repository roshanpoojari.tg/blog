@props(['heading'])
<section class="px-6 py-8">
    <h1 class="p-2 text-gray-600 text-center font-bold text-xl border-b-2 border-gray-200">
        {{ $heading }}
    </h1>


    <div class="flex">
        <aside class="w-48 m-4 border rounded p-4 flex-shrink-0">
            <h2 class="text-lg font-semibold mb-4">Links:</h2>
            <ul>
                <li class="border-b-2 rounded-xl p-2">
                    <a href="/admin/posts {{ request()->is('admin/posts') ? 'text-blue-500' : '' }}">All Posts</a>
                </li>
                <li class="border-b-2 rounded-xl p-2 {{ request()->is('admin/posts/create') ? 'text-blue-500' : '' }}">
                    <a href="/admin/posts/create">New Post</a>
                </li>
                <li class="border-b-2 rounded-xl p-2">
                    <a href="{{ route('home') }}">Home</a>
                </li>
            </ul>
        </aside>

        <main class="flex-1 mt-4">
            <x-panel>
                {{ $slot }}
            </x-panel>
        </main>
    </div>
</section>