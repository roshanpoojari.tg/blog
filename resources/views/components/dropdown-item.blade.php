@props(['active' => false])

@php
    $classes = 'block text-left px-3 text-sm leading-5 hover:bg-gray-300 rounded-xl';

    if ($active) $classes .= ' bg-gray-300 rounded-xl';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}
    >{{ $slot }}</a>