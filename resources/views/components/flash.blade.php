@if (session()->has('success'))
        <div
            x-data="{ show:true }"
            x-init="setTimeout(() => show = false, 4000)" 
            x-show="show"
            class="fixed bottom-3 right-3 bg-green-500 font-bold text-sm rounded-md text-white px-4 py-2">
            <p>{{ session('success') }}</p>
        </div>
@endif