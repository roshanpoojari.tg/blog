@props(['name', 'rows' => '2', 'placeholder' => ''])
<x-form.field>
    <x-form.label name="{{ $name }}"/> 
    <textarea 
        name="{{ $name }}" 
        id="description" 
        cols="30" 
        rows="{{ $rows }}" 
        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
        placeholder="{{ $placeholder }}">{{ $slot ?? old($name) }}</textarea>
    
    <x-form.error name="{{ $name }}"/>
</x-form.field>
</div>