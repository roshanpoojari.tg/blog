@props(['name'])
<x-form.field>
    <x-form.label :name="$name"/> 
    <input 
        name="{{ $name }}" 
        id="{{ $name }}"
        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
        placeholder="{{ $name }}"
        {{ $attributes(['value'=> old($name) ]) }}>
    <x-form.error :name="$name"/>
</x-form.field>