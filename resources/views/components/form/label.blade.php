@props(['name'])
<label for="{{ $name }}" class="block text-gray-700 text-sm font-bold mb-2">
    {{ ucwords($name) }}
</label>