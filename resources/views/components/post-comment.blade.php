@props(['comment'])
<x-panel class="bg-gray-50">
    <article class="flex">
        <div class="flex-shrink-0 mr-2 mt-2">
            <img class="rounded-full" src="https://i.pravatar.cc/60?u={{ $comment->user_id }}" alt="Profile" width="60" height="60">
        </div>
        <div class="mx-2 p-2">
            <header>
                <h3 class="font-bold text-lg">{{ $comment->author->name }}</h3>
                <p class="text-xs">Posted
                    <time>{{ $comment->created_at->diffForHumans() }}</time>
                </p>
            </header>
            <p class="text-sm font-semibold mt-2">
                {{ $comment->body }}
            </p>
        </div>
    </article>
</x-panel>