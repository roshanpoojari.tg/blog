<div {{ $attributes->merge(['class' => 'border border-gray-200 rounded-xl p-4']) }}>
    {{ $slot }}
</div>