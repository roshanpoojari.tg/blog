@auth
    <x-panel class="bg-gray-50">
        <form action="/posts/{{ $post->slug }}/comments" method="post">
            @csrf
            <header class="flex items-center mb-4">
                <img class="rounded-full mr-2" src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="Profile" width="40" height="40">
                <h2 class="ml-2">Want to participate?</h2>
            </header>

            <x-form.textarea name="body" rows="4" placeholder="Quick, Think of something to say!"/>

            <footer class="flex items-center justify-end mt-4">
                <x-form.button>Post</x-form.button>
            </footer>
        </form>
    </x-panel>
@else
    <x-panel>
        <h1 class="text-sm font-semibold">You can <a href="/login" class="text-blue-400 hover:underline">Login</a> or <a href="/register" class="text-blue-400 hover:underline">register</a> to comment on this post..</h1>
    </x-panel>
@endauth