<x-layout>
    @include('posts._header')
    <section class="px-6 py-8">
        <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
            
            @if ($posts->count())
                
                <x-posts-grid :posts="$posts"/>

                {{ $posts->links() }}
            @else
                <div class="m-3 p-4 rounded-full bg-gray-200 text-center">
                    <p>No posts yet. please come back later!</p>
                </div>
            @endif
        </main>
    </section>
</x-layout>