<x-layout>

    <x-setting heading="Publish New Post">
        <form action="/admin/posts" method="POST" enctype="multipart/form-data">
            @csrf
            <x-form.input name="title"/>
            <x-form.input name="slug"/>
            <x-form.input name="thumbnail" type="file"/>
            <x-form.textarea name="excerpt" rows="6" placeholder="Write something about your post.."/>
            <x-form.textarea name="body" rows="10" placeholder="Write your post here."/>
    
            
            <x-form.field>
                <x-form.label name="category"/>
                <select 
                    name="category_id" 
                    id="category" 
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
                    placeholder="Write your post here."
                    >
                    @foreach (\App\Models\Category::all() as $category)
                        <option 
                            value="{{ $category->id }}"
                            {{ old('category_id') == $category->id ? 'selected' : '' }}
                            >{{ ucwords($category->name) }}</option>
                    @endforeach
                </select>
                <x-form.error name="category" />
            </x-form.field>
    
            <x-form.button class="mt-4">
                Submit
            </x-form.button>
        </form>
    </x-setting>
</x-layout>