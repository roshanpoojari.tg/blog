<x-layout>

    <x-setting :heading="'Edit Post: '. $post->title" >
        <form action="/admin/posts/{{ $post->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <x-form.input name="title" :value="old('title', $post->title)"/>
            <x-form.input name="slug" :value="old('slug', $post->slug)"/>
            <div class="flex mt-4">
                <div class="flex-1">
                    <x-form.input name="thumbnail" type="file" :value="old('thumbnail', $post->thumbnail)"/>
                </div>
                <img src="{{ asset('storage/'. $post->thumbnail) }}" class="rounded-xl ml-4" width="100" alt="Thumbnail">
            </div>
            <x-form.textarea name="excerpt" rows="6" placeholder="Write something about your post..">{{ old('excerpt', $post->excerpt) }}</x-form.textarea>
            <x-form.textarea name="body" rows="10" placeholder="Write your post here.">{{ old('body', $post->body) }}</x-form.textarea>
    
            
            <x-form.field>
                <x-form.label name="category"/>
                <select 
                    name="category_id" 
                    id="category" 
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" 
                    placeholder="Write your post here."
                    >
                    @foreach (\App\Models\Category::all() as $category)
                        <option 
                            value="{{ $category->id }}"
                            {{ old('category_id', $post->category_id) == $category->id ? 'selected' : '' }}
                            >{{ ucwords($category->name) }}</option>
                    @endforeach
                </select>
                <x-form.error name="category" />
            </x-form.field>
    
            <x-form.button class="mt-4">
                Update
            </x-form.button>
        </form>
    </x-setting>
</x-layout>