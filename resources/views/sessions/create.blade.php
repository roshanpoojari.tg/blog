<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-xl mx-auto mt-10 bg-gray-200 border border-gray-300 rounded-xl px-10 py-5">
            <h1 class="p-2 text-gray-600 text-center font-bold text-xl">Login!</h1>
            <form class="mt-10" action="/sessions" method="POST">
                
                @csrf

                <x-form.input name="email" type="email" autocomplete="username"/>
                <x-form.input name="password" type="password" autocomplete="current-password"/>

                <div class="flex mb-6 justify-center w-full item-center">
                    <button 
                        class="py-2 px-4 text-white text-center font-bold text-xl w-1/2 rounded-full bg-blue-400 hover:bg-blue-500" 
                        type="submit">
                        Login
                    </button>
                </div>
            </form>
        </main>
    </section>
</x-layout>